var express = require("express");
var redis = require("redis");
var router = express.Router();
var { promisify } = require("util");
var { v4: uuidv4 } = require("uuid");

var redis_host = process.env.REDIS_HOST;

var client = redis.createClient(`redis://${redis_host}`);
var keysAsync = promisify(client.keys).bind(client);
var getAsync = promisify(client.get).bind(client);

async function getWorkerData(keyspace) {
  let keys = await keysAsync(keyspace);
  let data = {};
  for (const key of keys) {
    let name = key.split(":")[1];
    let pre_info = await getAsync(key);
    data[name] = JSON.parse(pre_info);
    if (data[name].status == "in_progress") {
      let pre_progress = await getAsync(`progress:${name}`);
      data[name].progress = JSON.parse(pre_progress);
    }
  }
  return data;
}

/* GET Current online workers */
router.get("/", (req, res, next) => {
  getWorkerData("worker:*").then((d) => {
    res.send({ workers: d });
  });
});

module.exports = router;

var express = require("express");
var router = express.Router();
var version = require("../version.json").version;

/* GET Version of Sisyphus server */
router.get("/", (req, res, next) => {
  res.send({ version: version });
});

module.exports = router;

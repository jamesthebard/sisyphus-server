var express = require('express');
var router = express.Router();
var version = require('../version.json').version

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Sisyphus Server', version: version });
});

module.exports = router;

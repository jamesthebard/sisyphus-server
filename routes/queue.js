var express = require("express");
var redis = require("redis");
var router = express.Router();
var { promisify } = require("util");
var { v4: uuidv4 } = require("uuid");

var redis_host = process.env.REDIS_HOST;

var client = redis.createClient(`redis://${redis_host}`);
var lrangeAsync = promisify(client.lrange).bind(client);
var lpushAsync = promisify(client.lpush).bind(client);
var deleteAsync = promisify(client.del).bind(client);

/* GET jobs in the queue */
router.get("/", (req, res, next) => {
  lrangeAsync("queue", 0, -1).then((depth) => {
    var queue = [];
    depth.reverse().forEach((value, index) => {
      data = JSON.parse(value);
      data.index = index;
      queue.push(data);
    });
    res.send(queue);
  });
});

/* POST job to the queue */
router.post("/", (req, res, next) => {
  job_uuid = uuidv4();
  req.body.job_id = job_uuid;
  lpushAsync("queue", JSON.stringify(req.body));
  res.send({ job_id: job_uuid });
});

/* GET reset queue */
router.get("/reset", (req, res, next) => {
  deleteAsync("queue").then(() => {
    res.send({ message: "Queue cleared" });
  });
});

module.exports = router;
